import { Lecturer } from '../models/lecturer';

export const LECTURERS: Lecturer[] = [
    {id: 4200, name: 'Bell Tata'},
    {id: 4201, name: 'Jam Mamy'},
    {id: 4202, name: 'On Nama'},
    {id: 4203, name: 'Pat Prapa'},
];
