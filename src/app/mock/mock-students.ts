import { Student } from '../models/student';

export const STUDENTS: Student[] = [
    {id: 614200, name: 'Bell Tata', email: 'bell@gmail.com'},
    {id: 614201, name: 'Jam Mamy', email: 'jam@gmail.com'},
    {id: 614202, name: 'On Nama', email: 'on@gmail.com'},
    {id: 614203, name: 'Pat Prapa', email: 'pat@gmail.com'},
];
