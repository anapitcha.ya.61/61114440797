import { Score } from '../models/score';

import { Student } from '../models/student';
import { STUDENTS } from '../mock/mock-students';

const students: Student[] = STUDENTS;

export const SCORES: Score[] = [
    {Student: students[0], score: 15, full_mark: 20},
    {Student: students[1], score: 12, full_mark: 20},
    {Student: students[2], score: 10, full_mark: 20},
    {Student: students[3], score: 18, full_mark: 20},

];
