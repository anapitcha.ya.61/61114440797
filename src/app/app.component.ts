import { Component } from '@angular/core';
import { Student } from './models/student';
import { STUDENTS } from './mock/mock-students';

import { Course } from './models/course';
import { COURSES } from './mock/mock-courses';

import { Lecturer } from './models/lecturer';
import { LECTURERS } from './mock/mock-lecturers';

import { Score } from './models/score';
import { SCORES } from './mock/mock-scores';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Course Web Application';
  description = 'คำอธิบายรายวิชา';
  students: Student[] = STUDENTS;
  courses: Course[] = COURSES;
  lecturers: Lecturer[] = LECTURERS;
  scores: Score[] = SCORES;
}

